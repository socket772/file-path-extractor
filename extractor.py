# Linux only, to be used with strace
# In strace paths are delimited within "</path/to/file/or/dir>"
# Remember to exlude empty ""
# 1 line 1 path
# add deduplication at the end (optional)

# REMINDER, ADD A LICENCE

### WARNING: THIS IS A TEST TO SEE PYTHON PERFORMANCE, MAYBE I WILL WRITE IT IN RUST OR C++

### Start here ###

import sys

# 1, open the file (passed as argument)
try:
	file_path = sys.argv[1]
except:
	print("Argument not passed")
	exit()

print(file_path)

# Check if empty
if file_path == "":
	print("Empty path")
	exit()

# 2 open the file, as text in read only
try:
	file = open(file_path, "rt")
except:
	print("File \"{}\" does not exist", file_path)
	exit()

# Save content to string
file_data_no_return = file.read().replace("\n", "")
print(file_data_no_return)